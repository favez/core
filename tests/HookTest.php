<?php

use \PHPUnit\Framework\TestCase;

class FavezFramework implements \Favez\Mvc\Hook\Hookable
{
    public function getName()
    {
        return 'favez';
    }
}

class HookTest extends TestCase
{

    public function testBasic()
    {
        $events     = \Favez\Mvc\App::events();
        $beforeHook = false;
        $afterHook  = false;

        $events->subscribe('FavezFramework.getName.before', function() use(&$beforeHook, &$afterHook) {
            $this->assertEquals(false, $beforeHook);
            $this->assertEquals(false, $afterHook);

            $beforeHook = true;
        });

        $events->subscribe('FavezFramework.getName.after', function(\Favez\Mvc\Event\Arguments $args) use(&$beforeHook, &$afterHook) {
            $this->assertEquals(true, $beforeHook);
            $this->assertEquals(false, $afterHook);

            $afterHook = true;

            $this->assertEquals('favez', $args->getReturn());

            $args->setReturn('favez framework');
        });

        $framework  = new FavezFramework();

        /** @var FavezFramework $proxy */
        $proxy      = new \Favez\Mvc\Hook\Proxy($framework);
        $return     = $proxy->getName();

        $this->assertEquals('favez framework', $return);
        $this->assertNotEquals('favez', $return);
    }

}
