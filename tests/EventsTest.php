<?php

use \PHPUnit\Framework\TestCase;
use \Favez\Mvc\App;

class EventsTest extends TestCase
{
    
    public function testBasic()
    {
        App::events()->subscribe('hello_world', function() {
            return 'hello world';
        });

        App::events()->subscribe('hello_world2', function(\Favez\Mvc\Event\Arguments $args) {
            $args->setReturn(false);
        });

        $this->assertEquals(null, App::events()->publish('hello_world'));

        $this->assertEquals(false, App::events()->publish('hello_world2'));
        $this->assertNotEquals(true, App::events()->publish('hello_world2'));
    }
    
    public function testCollector()
    {
        App::events()->subscribe('collect.messages', function() {
            return [
                1,
                2,
                3
            ];
        });
        
        App::events()->subscribe('collect.messages', function() {
            return [
                4,
                5,
                6
            ];
        });
        
        $this->assertEquals([1, 2, 3, 4, 5, 6], App::events()->collect('collect.messages'));
    }
    
}
