<?php

use Favez\Mvc\Config;

class ConfigTest extends \PHPUnit\Framework\TestCase
{
    
    public function testBasic()
    {
        $this->assertEquals(true, \Favez\Mvc\App::app()->config('test'));
        $this->assertInstanceOf(Config::class, \Favez\Mvc\App::app()->config());
    }
    
}