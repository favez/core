<?php

use PHPUnit\Framework\TestCase;
use Slim\Http\Body;

class DumpAndDieTest extends TestCase
{
    
    public function testDD()
    {
        $this->assertEquals('{"string":"hello world"}', (string) debug('hello world', true)->getBody());

        $this->assertEquals('{"boolean":true}', (string)debug(true, true)->getBody());

        $this->assertEquals('{"integer":1}', (string)debug(1, true)->getBody());

        $this->assertEquals('{"double":2.1}', (string)debug(2.1, true)->getBody());
    }

}