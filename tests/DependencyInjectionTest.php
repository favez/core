<?php

use Favez\Mvc\App;
use PHPUnit\Framework\TestCase;

class DependencyInjectionTest extends TestCase
{
    public function testInjection()
    {
        $this->assertInternalType('string', App::path());
        $this->assertInstanceOf(\Favez\Mvc\App::class, App::app());
        $this->assertInstanceOf(\Stash\Pool::class, App::cache());
        $this->assertInstanceOf(\Favez\Mvc\Http\Cookies::class, App::cookies());
        //$this->assertTrue($modules->db() instanceof \Favez\Mvc\Database\Database);
        $this->assertInstanceOf(\Favez\Mvc\Dispatcher::class, App::dispatcher());
        $this->assertInstanceOf(\Favez\Mvc\Event\Manager::class, App::events());
        $this->assertInstanceOf(\Favez\Mvc\Subscriber\Manager::class, App::subscriber());
        //$this->assertTrue($modules->models() instanceof \Favez\ORM\EntityManager);
        $this->assertInstanceOf(\Favez\Mvc\Http\Response\Json::class, App::json());
        $this->assertInstanceOf(\Slim\Http\Request::class, App::request());
        $this->assertInstanceOf(\Slim\Http\Response::class, App::response());
        $this->assertInstanceOf(\SlimSession\Helper::class, App::session());
        $this->assertInstanceOf(\Favez\Mvc\View\View::class, App::view());
        $this->assertInstanceOf(\Favez\Mvc\DI\Container::class, \Favez\Mvc\App::di());
    }
}
