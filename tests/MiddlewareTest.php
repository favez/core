<?php

use Favez\Mvc\Http\Response\Json;
use Favez\Mvc\Middleware\JsonResponseMiddleware;
use PHPUnit\Framework\TestCase;
use Slim\Http\Body;
use Slim\Http\Response;

class MiddlewareTest extends TestCase
{
    
    public function testJsonResponseMiddleware()
    {
        $result = $this->callMiddleware(JsonResponseMiddleware::class, function() {
            return ['success' => true ];
        });
        
        $this->assertEquals('{"success":true}', $result->getBody()->__toString());
        
        $result = $this->callMiddleware(JsonResponseMiddleware::class, function() {
            return (new Json())->success();
        });
        
        $this->assertEquals('{"success":true}', $result->getBody()->__toString());
        
        $result = $this->callMiddleware(JsonResponseMiddleware::class, function() {
            return new class {
                public $success = true;
            };
        });
    
        $this->assertEquals('{"success":true}', $result->getBody()->__toString());
    }
    
    private function callMiddleware($class, callable $callback)
    {
        $middleware = new $class();
        
        return $middleware(
            Favez\Mvc\App::request(),
            new Response(),
            $callback
        );
    }
    
}
