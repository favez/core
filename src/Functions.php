<?php

use Slim\Http\Response;

if (!function_exists('camelize')) {
    function camelize($scored)
    {
        return ucfirst(
            implode(
                '',
                array_map(
                    'ucfirst',
                    array_map(
                        'strtolower',
                        explode(
                            '_',
                            $scored
                        )
                    )
                )
            )
        );
    }
}

if (!function_exists('underscore')) {
    function underscore($cameled)
    {
        return implode(
            '_',
            array_map(
                'strtolower',
                preg_split('/([A-Z]{1}[^A-Z]*)/', $cameled, -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY)
            )
        );
    }
}

if (!function_exists('fetch_array')) {
    function fetch_array($key, $default = null, $pair = [], $delimiter = '.')
    {
        $keys = explode($delimiter, $key);

        foreach ($keys as $key) {
            if (isset($pair[$key])) {
                $pair = $pair[$key];
            } else {
                $pair = $default;
                break;
            }
        }

        return $pair;
    }
}

if (!function_exists('debug')) {
    /**
     * @param mixed $data
     * @param bool  $returnResponse
     *
     * @return mixed|Response
     */
    function debug($data, $returnResponse = false)
    {
        $middleware = new Favez\Mvc\Middleware\JsonResponseMiddleware();
        $result     = $middleware(
            Favez\Mvc\App::request(),
            new Slim\Http\Response(),
            static function () use ($data) {
                return $data;
            }
        );

        if ($returnResponse === true) {
            return $result;
        }

        Favez\Mvc\App::instance()->respond($result);
        die;
    }
}