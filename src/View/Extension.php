<?php

namespace Favez\Mvc\View;

use Favez\Mvc\App;
use Twig_Extension;
use Twig_SimpleFunction;

class Extension extends Twig_Extension
{
    /**
     * @var App
     */
    private $app;

    /**
     * @var View
     */
    private $view;

    public function __construct(App $app, View $view)
    {
        $this->app  = $app;
        $this->view = $view;
    }

    public function getName(): string
    {
        return 'View Extension';
    }

    public function getFunctions(): array
    {
        $functions = [ 'url', 'resource_url' ];
        $result    = [];

        foreach ($functions as $functionName) {
            $result[] = new Twig_SimpleFunction($functionName, [ $this, $functionName . 'Function' ]);
        }

        return $result;
    }

    public function resource_urlFunction($path)
    {
        return $this->urlFunction($this->view->themePath() . '_resources/' . $path);
    }

    public function urlFunction($path = ''): string
    {
        return App::urlService()->get($path);
    }
}