<?php

namespace Favez\Mvc\View;

use Favez\Mvc\DI\Injectable;
use Slim\Container;
use Twig_Environment;
use Twig_Loader_Filesystem;

class View
{
    use Injectable;

    public const DEFAULT_THEME = 'default';

    /**
     * @var Twig_Environment
     */
    protected $engine;

    /**
     * @var Twig_Loader_Filesystem
     */
    protected $loader;

    /**
     * @var array
     */
    protected $context;

    /**
     * @var array
     */
    protected $config;

    /**
     * @var string
     */
    protected $theme;

    public function __construct()
    {
        if (!self::app()->isTest() && !self::dispatcher()->dispatched()) {
            trigger_error(
                'The view component was created before a controller was dispatched so the view component 
            probably doesn\'t work as expected.',
                E_USER_NOTICE
            );
        }

        $this->config  = new Container(self::app()->config('view'));
        $this->context = [];
        $this->theme   =
            $this->config->has('theme') && isset($this->config->get('theme')[self::dispatcher()->moduleName()])
                ? $this->config->get('theme')[self::dispatcher()->moduleName()]
                : self::DEFAULT_THEME;

        $this->loader = new Twig_Loader_Filesystem();
        $this->engine = new Twig(
            $this->loader, [
            'autoescape'  => false,
            'auto_reload' => true,
            'cache'       => self::path() . $this->config->get('cache_path'),
        ]
        );

        $this->updatePaths();

        self::events()->publish('core.view.init', [ $this ]);

        $this->controllerName = self::dispatcher()->controllerName();
        $this->actionName     = self::dispatcher()->actionName();
    }

    public function updatePaths($overwrite = true): void
    {
        if (self::app()->isTest()) {
            return;
        }

        if ($overwrite) {
            $this->loader->setPaths([]);
        }

        $this->loader->addPath(self::path() . $this->getThemePath(), 'default');
        $this->loader->addPath(self::path() . $this->getThemePath($this->theme));
        $this->loader->addPath(self::path() . $this->getThemePath($this->theme), 'parent');
    }

    protected function getThemePath($themeName = self::DEFAULT_THEME): string
    {
        return $this->config()->get('theme_path') . self::dispatcher()->moduleName() . '/' . $themeName . '/';
    }

    public function config()
    {
        return $this->config;
    }

    public function setTheme(string $theme): void
    {
        $this->theme = $theme;

        $this->updatePaths();
    }

    public function loader(): Twig_Loader_Filesystem
    {
        return $this->loader;
    }

    public function exists($name)
    {
        return $this->loader->exists($this->getName($name));
    }

    protected function getName($name): string
    {
        if (strrpos($name, '.twig') !== strlen($name) - 5) {
            $name .= '.twig';
        }

        return $name;
    }

    public function render($name, $context = []): string
    {
        $name = $this->getName($name);

        self::events()->publish('core.view.render', [ $this, $name ]);

        return $this->engine()->render($name, array_merge($this->context, $context));
    }

    public function engine()
    {
        return $this->engine;
    }

    public function __get($name)
    {
        return $this->getAssign($name);
    }

    public function __set($name, $value)
    {
        $this->assign($name, $value);
    }

    public function assign($name, $value = []): View
    {
        if (is_array($name)) {
            $this->context = array_merge($this->context, $name);
        } else {
            $this->context[$name] = $value;
        }

        return $this;
    }

    public function getAssign($name, $default = null)
    {
        if (isset($this->context[$name])) {
            return $this->context[$name];
        }

        return $default;
    }

    public function clearAssign($name = null): View
    {
        if ($name === null) {
            $this->context = [];
        } elseif (isset($this->context[$name])) {
            unset($this->context[$name]);
        }

        return $this;
    }

    public function themePath(): string
    {
        return $this->getThemePath($this->theme);
    }
}