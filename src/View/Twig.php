<?php

namespace Favez\Mvc\View;

use Twig\Environment;

class Twig extends Environment
{
    private $extends = [];

    public function loadTemplate($name, $index = null)
    {
        if (isset($this->extends[$name]) && $this->extends[$name]['lock'] === false) {
            $template = $this->buildExtendedTemplate($name);

            $this->extends[$name]['lock'] = true;
            $template                     = $this->createTemplate($template);
            $this->extends[$name]['lock'] = false;

            return $template;
        }

        return parent::loadTemplate($name, $index);
    }

    private function buildExtendedTemplate($name): string
    {
        $template = '{% extends \'' . $name . '\' %}';

        foreach ($this->extends[$name]['children'] as $children) {
            $template .= "\r\n" . '{% use \'' . $children . '\' %}';
        }

        return $template;
    }

    public function extendsTemplate($parent, $children): void
    {
        if (!isset($this->extends[$parent])) {
            $this->extends[$parent] = [
                'parent'   => $parent,
                'lock'     => false,
                'children' => [ $children ],
            ];
        } else {
            $this->extends[$parent]['children'][] = $children;
        }
    }
}