<?php

namespace Favez\Mvc\ORM;

use Favez\Mvc\App;

abstract class Entity extends \Favez\ORM\Entity\Entity
{
    public static function create()
    {
        return self::repository()->create();
    }

    public function save()
    {
        self::repository()->save($this);
    }

    public static function repository(): \Favez\ORM\Entity\Repository
    {
        return App::models()->getRepository(static::class);
    }

    /**
     * @deprecated Use remove() instead
     */
    public function delete(): void
    {
        $this->remove();
    }

    public function remove(): void
    {
        self::repository()->remove($this);
    }
}
