<?php

namespace Favez\Mvc\Exception;

use Exception;

class InvalidSubscriberException extends Exception
{
}