<?php

namespace Favez\Mvc\Exception;

use Exception;

class AccessViolationException extends Exception
{
}