<?php

namespace Favez\Mvc\Exception;

use Exception;

class InvalidTargetException extends Exception
{
}