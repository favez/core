<?php

namespace Favez\Mvc\Subscriber;

use Favez\Mvc\DI\Injectable;

abstract class Subscriber
{
    use Injectable;

    final public function __construct()
    {
    }

    abstract public function getSubscribedEvents();
}