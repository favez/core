<?php

namespace Favez\Mvc\Subscriber;

use Exception;
use Favez\Mvc\App;
use Favez\Mvc\Exception\InvalidSubscriberException;

class Manager
{
    /**
     * @var App
     */
    protected $app;

    protected $registered = false;

    public function __construct(App $app)
    {
        $this->app        = $app;
        $this->registered = false;
    }

    public function register(): void
    {
        if ($this->isRegistered()) {
            throw new Exception('Can not execute SubscriberManager::register twice.');
        }

        $subscribers = $this->app->config('subscribers', []);

        foreach ($subscribers as $subscriber) {
            if (!($subscriber instanceof Subscriber)) {
                throw new InvalidSubscriberException();
            }

            $this->initialize($subscriber);
        }

        $this->setRegistered(true);
    }

    public function isRegistered(): bool
    {
        return $this->registered;
    }

    public function setRegistered($registered): void
    {
        $this->registered = (bool)$registered;
    }

    public function initialize(Subscriber $subscriber): void
    {
        foreach ($subscriber->getSubscribedEvents() as $name => $callable) {
            App::events()->subscribe($name, $callable);
        }
    }
}