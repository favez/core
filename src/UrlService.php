<?php

namespace Favez\Mvc;

use Slim\Http\Request;

/**
 * Class UrlService
 *
 * @category PHP_Framework
 * @package  Favez\Mvc
 * @author   tyurderi <tyurderi@yahoo.de>
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     http://github.com/favez/favez
 */
class UrlService
{
    private $_request;

    /**
     * UrlService constructor.
     *
     * @param Request $request The slim http request
     */
    public function __construct(Request $request)
    {
        $this->_request = $request;
    }

    /**
     * Get an url.
     *
     * @param string $path           The path that will be appended to the base url.
     * @param bool   $removePassword Whether the auth password should be removed.
     *
     * @return mixed|string
     */
    public function get($path = '', $removePassword = true)
    {
        $url = $this->_request->getUri()->getBaseUrl() . '/' . $path;

        if ($removePassword === true) {
            $start = strpos($url, '//') + 1;
            $end   = strpos($url, '@');

            if ($start !== false && $end !== false) {
                $url = str_replace(
                    substr($url, $start + 1, $end - $start),
                    '',
                    $url
                );
            }
        }

        return $url;
    }
}