<?php

namespace Favez\Mvc\Event;

class Manager
{
    protected $listeners = [];

    public function subscribe($name, $callable, $priority = 0): bool
    {
        if (is_callable($callable)) {
            if (!isset($this->listeners[$name])) {
                $this->listeners[$name] = [];
            }

            $this->listeners[$name][$priority][] = $callable;

            ksort($this->listeners[$name], SORT_ASC);

            return true;
        }

        return false;
    }

    /**
     * Collects data from event listeners.
     *
     * @param string          $name
     * @param array|Arguments $arguments
     *
     * @return array
     */
    public function collect($name, $arguments = []): array
    {
        $items = [];

        if (!($arguments instanceof Arguments)) {
            $arguments = new Arguments($arguments);
        }

        if ($listeners = $this->getListeners($name)) {
            foreach ($listeners as $listenerContainer) {
                foreach ($listenerContainer as $listener) {
                    $item = $listener($arguments);

                    if (is_array($item) === true) {
                        $items = array_merge_recursive($items, $item);
                    }

                    if ($arguments->processed()) {
                        break;
                    }
                }
            }
        }

        return $items;
    }

    public function getListeners($name)
    {
        if (!$this->hasListeners($name)) {
            return null;
        }

        return $this->listeners[$name];
    }

    public function hasListeners($name): bool
    {
        return isset($this->listeners[$name]) && !empty($this->listeners[$name]);
    }

    /**
     * @param string          $name
     * @param array|Arguments $arguments
     *
     * @return mixed
     */
    public function publish($name, $arguments = [])
    {
        if (!($arguments instanceof Arguments)) {
            $arguments = new Arguments($arguments);
        }

        if ($listeners = $this->getListeners($name)) {
            foreach ($listeners as $listenerContainer) {
                foreach ($listenerContainer as $listener) {
                    $listener($arguments);

                    if ($arguments->processed()) {
                        break;
                    }
                }
            }
        }

        return $arguments->getReturn();
    }

    /**
     * @param string|null $name
     *
     * @return array|null
     * @deprecated
     */
    public function listeners($name = null): ?array
    {
        trigger_error(
            __CLASS__ . '::listeners() is deprecated. Use hasListeners, getListeners or getAllListeners instead.'
        );

        if (empty($name)) {
            return $this->getAllListeners();
        } elseif ($this->hasListeners($name)) {
            return $this->getListeners($name);
        }

        return null;
    }

    public function getAllListeners(): array
    {
        return $this->listeners;
    }
}