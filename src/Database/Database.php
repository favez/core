<?php

namespace Favez\Mvc\Database;

use Favez\Mvc\App;
use Favez\Mvc\DI\Injectable;
use FluentPDO;
use PDO;

class Database
{
    use Injectable;

    /**
     * @var FluentPDO
     */
    protected $db;

    /**
     * @var PDO
     */
    protected $pdo;

    public function __construct(App $app)
    {
        $this->pdo = new PDO(
            sprintf(
                'mysql:host=%s;dbname=%s',
                $app->config('database.host'),
                $app->config('database.shem')
            ),
            $app->config('database.user'),
            $app->config('database.pass')
        );

        $this->db = new FluentPDO($this->pdo);

        self::events()->publish('core.database.init', [ $this ]);

        if (self::app()->isDebug()) {
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
    }

    public function query($statement)
    {
        return $this->pdo()->prepare($statement);
    }

    public function pdo(): PDO
    {
        return $this->db->getPdo();
    }

    public function delete($table, $primaryKey = null): \DeleteQuery
    {
        return $this->db->delete($table, $primaryKey);
    }

    public function insert($table, $values = []): \InsertQuery
    {
        return $this->db->insertInto($table, $values);
    }

    public function update($table, $set = [], $primaryKey = null): \UpdateQuery
    {
        return $this->db->update($table, $set, $primaryKey);
    }

    public function from($table, $primaryKey = null): \SelectQuery
    {
        return $this->db->from($table, $primaryKey);
    }
}