<?php

namespace Favez\Mvc;

use ReflectionClass;

trait Singleton
{
    /**
     * @var mixed
     */
    protected static $instance;

    public static function instance()
    {
        if (self::$instance === null) {
            $reflectionClass = new ReflectionClass(__CLASS__);
            self::$instance  = $reflectionClass->newInstanceArgs(func_get_args());
        }

        return self::$instance;
    }
}