<?php

namespace Favez\Mvc\Http\Response;

use Favez\Mvc\Singleton;
use Slim\Http\Response;

class Json
{
    use Singleton;

    private $data;

    public function __construct()
    {
        $this->data = [];
    }

    /**
     * @param array|string $name
     * @param mixed        $value
     *
     * @return $this
     * @deprecated Use Json::assign instead.
     */
    public function add($name, $value = null): self
    {
        return $this->assign($name, $value);
    }

    public function assign($name, $value = null): self
    {
        if (is_array($name)) {
            foreach ($name as $key => $value) {
                $this->data[$key] = $value;
            }
        } elseif (is_string($name)) {
            $this->data[$name] = $value;
        } elseif (is_object($name)) {
            $this->data = (array)$name;
        }

        return $this;
    }

    public function remove($key): self
    {
        if ($this->has($key)) {
            unset($this->data[$key]);
        }

        return $this;
    }

    public function has($key): bool
    {
        return isset($this->data[$key]);
    }

    public function success($data = []): Response
    {
        $this->assign($data);
        $this->assign('success', true);

        return $this->send();
    }

    public function send(): Response
    {
        return (new Response())
            ->withStatus(200)
            ->withHeader('Content-Type', 'application/json')
            ->write(
                json_encode(
                    $this->data
                )
            );
    }

    public function failure($data = []): Response
    {
        $this->assign($data);
        $this->assign('success', false);

        return $this->send();
    }
}