<?php

namespace Favez\Mvc\Http;

class Cookies implements CookiesInterface
{
    public function get($key, $default = null)
    {
        return $_COOKIE[$key] ?? $default;
    }

    public function reset($key): void
    {
        $this->set($key, '', 0);
    }

    /**
     * @param      $key
     * @param      $value
     * @param null $expire
     * @param null $path
     * @param null $domain
     * @param null $secure
     * @param null $httpOnly
     *
     * @return bool
     */
    public function set(
        $key,
        $value,
        $expire = null,
        $path = null,
        $domain = null,
        $secure = null,
        $httpOnly = null
    ): bool {
        return setcookie($key, $value, $expire, $path, $domain, $secure, $httpOnly);
    }
}