<?php

namespace Favez\Mvc;

use Favez\Mvc\DI\Injectable;
use Favez\Mvc\Event\Arguments;
use Favez\Mvc\Exception\InvalidTargetException;
use Favez\Mvc\Hook\Hookable;
use Favez\Mvc\Hook\Proxy;

class Dispatcher
{
    use Injectable;

    protected $moduleName     = '';

    protected $controllerName = '';

    protected $actionName     = '';

    protected $dispatched     = false;

    public function forward($module, $controller, $action = 'index', $params = [], $update = false)
    {
        if ($update && $this->moduleName !== $module) {
            $this->moduleName = $module;

            $this->view()->updatePaths();
        }

        return $this->dispatch([ $module, $controller, $action ], $params);
    }

    public function dispatch($target, $params)
    {
        [ $className, $methodName ] = $this->getControllerByTarget($target, $params);

        $this->dispatched = true;

        $this->events()->publish('controller.resolve.' . $this->moduleName . '.' . $this->controllerName);

        if (class_exists($className) && method_exists($className, $methodName)) {
            $this->events()->publish('controller.pre_dispatch.' . $this->moduleName);
            $this->events()->publish('controller.pre_dispatch.' . $this->moduleName . '.' . $this->controllerName);

            $controller = $this->createController($className);
            $result     = null;

            $controller->preDispatch();

            $eventName =
                'controller.dispatch.' . $this->moduleName . '.' . $this->controllerName . '.' . $this->actionName;
            $args      = new Arguments([]);
            $result    = $this->events()->publish($eventName, $args);

            if (!$args->processed()) {
                $result = $controller->$methodName($params);
            }

            $controller->postDispatch();

            $this->events()->publish('controller.post_dispatch.' . $this->moduleName . '.' . $this->controllerName);
            $this->events()->publish('controller.post_dispatch.' . $this->moduleName);

            return $result;
        }

        return $this->app()->getContainer()->get('notFoundHandler')(
            $this->app()->request(),
            $this->app()->response()
        );
    }

    protected function getControllerByTarget($target, $params)
    {
        if (is_string($target)) {
            [ $module, $controller, $action ] = explode(':', $this->applyParams($target, $params));
        } elseif (is_array($target)) {
            [ $module, $controller, $action ] = $target;

            if (empty($module) || empty($controller) || empty($action)) {
                throw new InvalidTargetException();
            }
        } else {
            throw new InvalidTargetException();
        }

        $this->moduleName     = $module;
        $this->controllerName = $controller;
        $this->actionName     = $action;

        $controllerConfig = $this->app()->config('modules.' . $module . '.controller');
        $controllerClass  = sprintf(
            '%s%s%s',
            $controllerConfig['namespace'],
            $controller,
            $controllerConfig['class_suffix']
        );

        $methodName = sprintf('%s%s', $action, $controllerConfig['method_suffix']);

        return [ $controllerClass, $methodName ];
    }

    protected function applyParams($target, $params)
    {
        $params = array_merge(
            [
                'module'     => 'frontend',
                'controller' => 'index',
                'action'     => 'index',
            ],
            $params
        );

        $params['controller'] = ucfirst($params['controller']);

        foreach ($params as $key => $value) {
            $target = str_replace('{' . $key . '}', $value, $target);
        }

        return $target;
    }

    /**
     * @param string $className
     *
     * @return Controller|Proxy
     */
    protected function createController($className)
    {
        /** @var Controller $controller */
        $controller = new $className();

        if (in_array(Hookable::class, class_implements($className), true)) {
            $controller = new Proxy($controller);
        }

        return $controller;
    }

    public function dispatched(): bool
    {
        return $this->dispatched;
    }

    public function moduleName(): string
    {
        return $this->moduleName;
    }

    public function controllerName(): string
    {
        return $this->controllerName;
    }

    public function actionName(): string
    {
        return $this->actionName;
    }
}