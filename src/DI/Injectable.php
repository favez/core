<?php

namespace Favez\Mvc\DI;

use Favez\Mvc\App;
use Favez\Mvc\Database\Database;
use Favez\Mvc\Dispatcher;
use Favez\Mvc\Http\Cookies;
use Favez\Mvc\Http\Response\Json;
use Favez\Mvc\Subscriber\Manager;
use Favez\Mvc\UrlService;
use Favez\Mvc\View\View;
use Slim\Http\Request;
use Slim\Http\Response;
use SlimSession\Helper;
use Stash\Pool;

/**
 * Trait Injectable
 *
 * @package Favez\Mvc\DI
 *
 * @method static string                   path()
 * @method static Pool                     cache()
 * @method static Database                 db()
 * @method static View                     view(array $configuration = [])
 * @method static \Favez\ORM\App           models()
 * @method static Json                     json()
 * @method static \Favez\Mvc\Event\Manager events()
 * @method static Manager                  subscriber()
 * @method static Dispatcher               dispatcher()
 * @method static Helper                   session()
 * @method static Cookies                  cookies()
 * @method static UrlService               urlService()
 *
 * @method static mixed  config($key, $default = null)
 * @method static string url($path = '')
 *
 * @method static App       app()
 * @method static Request   request()
 * @method static Response  response()
 */
trait Injectable
{
    public static function __callStatic($name, $arguments)
    {
        return App::di()->get($name, $arguments);
    }

    public function __call($name, $arguments)
    {
        return App::di()->get($name, $arguments);
    }
}