<?php

namespace Favez\Mvc\DI;

use Closure;

class Item
{
    protected $name;

    protected $callable;

    protected $shared;

    public function __construct($name, $callable, $shared)
    {
        $this->name     = $name;
        $this->callable = $callable;
        $this->shared   = $shared;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return Closure
     */
    public function getCallable(): callable
    {
        return $this->callable;
    }

    /**
     * @return boolean
     */
    public function isShared(): bool
    {
        return $this->shared;
    }
}