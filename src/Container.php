<?php

namespace Favez\Mvc;

use ArrayAccess;

class Container implements ArrayAccess
{
    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function __get($key)
    {
        return $this->get($key);
    }

    public function __set($key, $value)
    {
        $this->set($key, $value);
    }

    public function get($key, $default = null)
    {
        if (isset($this->data[$key])) {
            return $this->data[$key];
        }

        return $default;
    }

    public function set($key, $value)
    {
        $this->data[$key] = $value;
    }

    public function offsetExists($key): bool
    {
        return $this->has($key);
    }

    public function has($key): bool
    {
        return isset($this->data[$key]);
    }

    public function offsetSet($key, $value): void
    {
        $this->set($key, $value);
    }

    public function offsetGet($key)
    {
        return $this->get($key);
    }

    public function offsetUnset($key): void
    {
        $this->reset($key);
    }

    public function reset($key): void
    {
        if (isset($this->data[$key])) {
            unset($this->data[$key]);
        }
    }
}