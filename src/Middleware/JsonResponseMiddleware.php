<?php

namespace Favez\Mvc\Middleware;

use Favez\Mvc\Http\Response\Json;
use Slim\Http\Request;
use Slim\Http\Response;

class JsonResponseMiddleware
{
    public function __invoke(Request $request, Response $response, callable $next)
    {
        $response = $next($request, $response);

        if ($response instanceof Response) {
            return $response;
        }

        if ($response instanceof Json) {
            return $response->send();
        }

        if (is_array($response)) {
            return (new Json())
                ->assign($response)
                ->send();
        }

        if (is_object($response)) {
            return (new Json())
                ->assign($response)
                ->send();
        }

        return (new Json())
            ->assign(gettype($response), $response)
            ->send();
    }
}