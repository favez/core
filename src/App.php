<?php

namespace Favez\Mvc;

use Composer\Autoload\ClassLoader;
use Favez\Mvc\DI\Injectable;
use Favez\Mvc\Exception\InvalidTargetException;
use Favez\Mvc\Http\Cookies;
use Favez\ORM\EntityManager;
use Psr\Http\Message\ResponseInterface;
use Slim\Interfaces\RouteInterface;
use Slim\Middleware\Session;
use SlimSession\Helper;
use Stash\Driver\FileSystem;
use Stash\Pool;
use Throwable;
use Twig_Extension_Debug;

/**
 * Class App
 *
 * @category PHP_Framework
 * @package  Favez\Mvc
 * @author   tyurderi <tyurderi@yahoo.de>
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     http://github.com/favez/favez
 *
 * @method static App instance(array $container = [])
 */
class App extends \Slim\App
{
    use Singleton;
    use Injectable;

    private static $_di;

    /**
     * The composer autoload class.
     * It can be null if it was not passed to the application by self::setLoader.
     *
     * @var ClassLoader
     */
    private $_loader;

    /**
     * The config container
     *
     * @var Config
     */
    private $config;

    /**
     * App constructor.
     *
     * @param array $container Basic container with app configuration.
     *
     * @throws Exception\InvalidSubscriberException
     */
    public function __construct($container = [])
    {
        if (isset($container['config'])) {
            $config = $container['config'];

            unset ($container['config']);

            if (!($config instanceof Config)) {
                $config = new Config($config);
            }

            $this->config = $config;
        }

        parent::__construct($container);

        // Initialize session if required
        if ($this->config('cookie.enabled') === true) {
            $config  = $this->config('cookie.config');
            $session = new Session($config);

            $this->add($session);
        }

        $this->initDIContainer();

        self::subscriber()->register();
        self::events()->publish('core.app.init');
    }

    /**
     * Get a configuration item by array key path.
     *
     * @param string $key     The key of the config item.
     * @param mixed  $default Default value if no config item found.
     *
     * @return mixed
     */
    public function config($key = null, $default = null)
    {
        if ($key === null) {
            return $this->config;
        }

        return fetch_array($key, $default, $this->config);
    }

    /**
     * Initialize all default dependency container items.
     *
     * @return void
     */
    protected function initDIContainer(): void
    {
        self::di()->registerShared(
            [
                'path'       => function () {
                    return $this->config('app.path');
                },
                'cache'      => function () {
                    return new Pool(
                        new FileSystem(
                            [
                                'path'            => $this->config('app.path') . $this->config('app.cache_path'),
                                'filePermissions' => 0777,
                                'dirPermissions'  => 0777,
                            ]
                        )
                    );
                },
                'db'         => function () {
                    return new Database\Database($this);
                },
                'view'       => function () {
                    $view = new View\View();

                    if ($this->isDebug()) {
                        $view->engine()->enableDebug();
                        $view->engine()->addExtension(new Twig_Extension_Debug());
                    }

                    $view->engine()->addExtension(new View\Extension($this, $view));

                    return $view;
                },
                'models'     => function () {
                    return new \Favez\ORM\App(self::db()->pdo());
                },
                'json'       => function () {
                    return new Http\Response\Json();
                },
                'events'     => function () {
                    return new Event\Manager();
                },
                'subscriber' => function () {
                    return new Subscriber\Manager($this);
                },
                'dispatcher' => function () {
                    return new Dispatcher();
                },
                'session'    => function () {
                    return new Helper();
                },
                'cookies'    => function () {
                    return new Cookies();
                },
                'urlService' => function () {
                    return new UrlService(self::request());
                },
            ]
        );

        self::di()->register(
            [
                'app'      => function () {
                    return $this;
                },
                'request'  => function () {
                    return $this->getContainer()->get('request');
                },
                'response' => function () {
                    return $this->getContainer()->get('response');
                },
                'config'   => function ($key, $default = null) {
                    return $this->config($key, $default);
                },
                'url'      => function ($path = '') {
                    return self::urlService()->get($path);
                },
            ]
        );
    }

    /**
     * Gets the dependency injection container.
     *
     * @return DI\Container
     */
    public static function di()
    {
        if (self::$_di === null) {
            self::$_di = new DI\Container();
        }

        return self::$_di;
    }

    /**
     * Whether the app is in debug mode or not. Usually used for development.
     *
     * @return bool
     */
    public function isDebug(): bool
    {
        return $this->config('debug') === true;
    }

    /**
     * A helper class to get dependency injection modules.
     *
     * @return     Modules
     * @deprecated Use static module access through this class instead.
     */
    public static function modules(): Modules
    {
        return Modules::instance();
    }

    /**
     * Run the app.
     *
     * @param bool $silent {inheritdoc}
     *
     * @return ResponseInterface
     * @throws Throwable
     */
    public function run($silent = false): ResponseInterface
    {
        self::events()->publish('core.app.run');

        return parent::run($silent);
    }

    /**
     * Route to target mapping.
     *
     * @param array           $methods Allowed HTTP methods.
     * @param string          $pattern The route pattern.
     * @param callable|string $target  The target that should be called.
     *
     * @return RouteInterface
     * @throws InvalidTargetException
     */
    public function map(array $methods, $pattern, $target): ?RouteInterface
    {
        if (is_string($target) || is_array($target)) {
            return parent::map(
                $methods,
                $pattern,
                function ($request, $response, $params) use ($target) {
                    return self::dispatcher()->dispatch(
                        $target,
                        $params
                    );
                }
            );
        }

        if (is_callable($target)) {
            return parent::map($methods, $pattern, $target);
        }

        throw new InvalidTargetException();
    }

    /**
     * Whether the app is in test mode or not. Usually used by phpunit.
     *
     * @return bool
     */
    public function isTest(): bool
    {
        return $this->config('test') === true;
    }

    /**
     * Get the correct url by path.
     *
     * @param string $path The path after the url.
     *
     * @return mixed
     *
     * @deprecated Use UrlService::get or Injectable::url instead
     */
    public function url($path = '')
    {
        return self::urlService()->get($path);
    }

    /**
     * Set the app autoloader from composer.
     *
     * @param ClassLoader $_loader The loader instance.
     *
     * @return void
     */
    public function setLoader($_loader): void
    {
        $this->_loader = $_loader;
    }

    /**
     * Get the apps composer autoloader.
     *
     * @return ClassLoader
     */
    public function loader(): ClassLoader
    {
        return $this->_loader;
    }
}