<?php

namespace Favez\Mvc;

use Favez\Mvc\DI\Injectable;

abstract class Controller
{
    use Injectable;

    protected $moduleName     = '';

    protected $controllerName = '';

    public function __construct()
    {
        $this->moduleName     = $this->dispatcher()->moduleName();
        $this->controllerName = $this->dispatcher()->controllerName();
    }

    public function forward($action = 'index', $controller = null, $module = null, $params = [], $update = false)
    {
        $module     = $module ? $module : $this->moduleName;
        $controller = $controller ? $controller : $this->controllerName;

        return $this->dispatcher()->forward($module, $controller, $action, $params, $update);
    }

    /**
     * This method will be called before every dispatch of this controller.
     */
    public function preDispatch()
    {
    }

    /**
     * This method will be called after every successful dispatch of this controller.
     */
    public function postDispatch()
    {
    }
}